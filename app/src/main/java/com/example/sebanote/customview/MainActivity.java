package com.example.sebanote.customview;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MyCustomBtnSwitch.MyCustomBtnSwitchListener {

    private MyCustomBtnSwitch myCustomBtnSwitch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myCustomBtnSwitch = findViewById(R.id.my_custom_pill);
        myCustomBtnSwitch.setListener(this);
    }

    @Override
    public void onBtnClick(int pos) {
        Toast.makeText(getApplicationContext(), "btn click number " + pos, Toast.LENGTH_SHORT).show();
    }
}

