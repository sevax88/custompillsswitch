package com.example.sebanote.customview;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyCustomBtnSwitch extends LinearLayout {

    public static final String ACTUAL_POS = "actualPos";
    public static final int DURATION = 450;
    private TextView option1Btn, option2Btn;
    private FrameLayout selection;
    private AccelerateInterpolator accelerateInterpolator = new AccelerateInterpolator();
    private ArgbEvaluator argbEvaluator = new ArgbEvaluator();
    private int actualPos;
    private MyCustomBtnSwitchListener listener;

    public MyCustomBtnSwitch(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray attr = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomPill,
                0, 0);
        String btnTextOne = attr.getString(R.styleable.CustomPill_text_first_btn);
        String btnTextTwo = attr.getString(R.styleable.CustomPill_text_second_btn);
        boolean isSecondBtnClickleable = attr.getBoolean(R.styleable.CustomPill_is_second_btn_clickleable,false);
        init(context, btnTextOne, btnTextTwo, isSecondBtnClickleable);
    }
    private void init(Context context, String btnTextOne, String btnTextTwo, boolean isSecondBtnClickleable) {
        LayoutInflater.from(context).inflate(R.layout.my_custom_view,this);
        setBackgroundColor(getResources().getColor(R.color.salmon_20));
        option1Btn = findViewById(R.id.textView);
        option2Btn = findViewById(R.id.textView2);
        if (!isSecondBtnClickleable) disableSecondBtn();
        selection = findViewById(R.id.frame);
        option1Btn.setText(btnTextOne);
        option2Btn.setText(btnTextTwo);
        option1Btn.setOnClickListener(v-> {if (isSecondBtnClickleable)startAnimation(option1Btn, option2Btn); actualPos = 0; if (listener != null)listener.onBtnClick(0);});
        option2Btn.setOnClickListener(v-> {if (isSecondBtnClickleable)startAnimation(option2Btn, option1Btn); actualPos = 1; if (listener != null)listener.onBtnClick(1);});
    }

    private void disableSecondBtn() {
        option2Btn.setEnabled(false);
        option2Btn.setTextColor(getResources().getColor(R.color.gray));
    }

    private void startAnimation(TextView selected, TextView unselected){

       ObjectAnimator horizontalAnimator = ObjectAnimator.ofFloat(selection, "translationX", selection.getTranslationX(), selected.getLeft())
               .setDuration(DURATION);
       horizontalAnimator.setInterpolator(accelerateInterpolator);
       horizontalAnimator.start();

       ObjectAnimator colorAnimatorSelected = ObjectAnimator.ofInt(selected, "textColor", selected.getCurrentTextColor(),getResources().getColor(android.R.color.white))
               .setDuration(DURATION);
       colorAnimatorSelected.setEvaluator(argbEvaluator);
       colorAnimatorSelected.start();

       ObjectAnimator colorAnimatorUnselected = ObjectAnimator.ofInt(unselected, "textColor", unselected.getCurrentTextColor(),getResources().getColor(R.color.navy))
                .setDuration(DURATION);
       colorAnimatorUnselected.setEvaluator(argbEvaluator);
       colorAnimatorUnselected.start();

    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putInt(ACTUAL_POS, actualPos);
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(null);
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            actualPos = bundle.getInt(ACTUAL_POS);
            if (actualPos == 1) {
                getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        selection.setX(option2Btn.getX());
                        option2Btn.setTextColor(getResources().getColor(android.R.color.white));
                        option1Btn.setTextColor(getResources().getColor(R.color.oxford_70));
                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
        }
    }

    public void setListener(MyCustomBtnSwitchListener listener) {
        this.listener = listener;
    }

    interface MyCustomBtnSwitchListener {
        void onBtnClick(int pos);
    }
}
